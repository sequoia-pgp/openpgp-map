.SUFFIXES: .dot .png .svg

.dot.png:
	dot -Tpng map.dot -o map.png

.dot.svg:
	dot -Tsvg map.dot -o map.svg

all: map.png map.svg

map.png map.svg: map.dot Makefile
